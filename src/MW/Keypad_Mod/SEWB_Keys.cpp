

#include "SEWB_keys.h"

//  1    2    3      4     5     6     7     8    9
// NC  COL2  ROW0  COL1  ROW3  COL3  ROW2  ROW1  NC
// NC  GRY   WHT   BLK   BRN   RED   ORG   YLW   NC

SEWB_keys::SEWB_keys(void)
{
	keypad = new Keypad(makeKeymap(keys), rowPins, colPins, Keypad_Row, Keypad_Col );
}

SEWB_keys::~SEWB_keys(void)
{
	delete keypad;
}

bool SEWB_keys::SEWB_KEY_Get_Key_Pressed(uint8_t* Key_Val)
{
	bool rReturn = false;
	uint8_t key = 0;

	key = this->keypad->getKey();
	if(key)
	{
		*Key_Val = key;
		rReturn = true;
	}
	return rReturn;
}