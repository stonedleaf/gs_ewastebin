/*
* Wifi Library
* 
* Author: Jim I.
* April 2021
*
*/

#ifndef __SEWB_WIFI_H__
#define __SEWB_WIFI_H__

#define HW_RESET_RETRIES 3
#define HW_WIFI_BAUDRATE 115200

#include "../../PHY/AM2560_IF.h"
#include "../Serial_Mod/SEWB_Serial.h"
#include <avr/pgmspace.h>

class SEWB_Wifi : public Print {

    public:
        SEWB_Wifi(SEWB_Serial *serialWifi, SEWB_Serial *serialDebug = NULL, int8_t resetPin = -1);
        bool begin();
        //bool connectToAP(Fstr *ssid, Fstr *pass);

    private:
		SEWB_Serial* _serialWifi;
        SEWB_Serial* _serialDebug;
        int8_t _resetPin;

        bool _writingToDebug;

        byte readCommand(int timeout, const char* text1 = NULL, const char* text2 = NULL);
        virtual size_t write(uint8_t);
};

#endif /* __SEWB_WIFI_H__ */
