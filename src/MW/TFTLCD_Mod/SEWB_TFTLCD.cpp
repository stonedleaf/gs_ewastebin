

#include "SEWB_TFTLCD.h"
#include <SPI.h>          // f.k. for Arduino-1.5.2

#define LCD_CS 			A3     // Chip Select goes to Analog 3
#define LCD_CD 			A2     // Command/Data goes to Analog 2
#define LCD_WR 			A1     // LCD Write goes to Analog 1
#define LCD_RD 			A0     // LCD Read goes to Analog 0
#define LCD_RESET 		A4  // Can alternately just connect to Arduino's reset pin

// Assign human-readable names to some common 16-bit color values:
#define BLACK   		0x0000
#define BLUE    		0x001F
#define RED     		0xF800
#define GREEN   		0x07E0
#define CYAN    		0x07FF
#define MAGENTA 		0xF81F
#define YELLOW  		0xFFE0
#define WHITE   		0xFFFF

SEWB_Tftlcd::SEWB_Tftlcd(void)
{
	/* Nothing Goes Here */
}

SEWB_Tftlcd::~SEWB_Tftlcd(void)
{
	/* Nothing Goes Here */
}

bool SEWB_Tftlcd::SEWB_Tftlcd_Begin(void)
{
	bool rResult = false;
	
	LCD_ID = SEWB_Tft_Lcd.readID();
    if (LCD_ID == 0xD3D3) 
    {
		LCD_ID = 0x9481;
    }
	SEWB_Tft_Lcd.begin(LCD_ID);
	
	return rResult = true;
}

bool SEWB_Tftlcd::SEWB_Tftlcd_Reset(void)
{
	bool rResult = false;
	
	SEWB_Tft_Lcd.reset();
	
	return rResult = true;
}

void SEWB_Tftlcd::SEWB_Tftlcd_FillScreen(int bColor)
{
	SEWB_Tft_Lcd.fillScreen(bColor);
	
}

void SEWB_Tftlcd::SEWB_Tftlcd_Write(String Txt, int TxtSize, int TxtColor)
{
    SEWB_Tft_Lcd.setTextColor(TxtColor);  
    SEWB_Tft_Lcd.setTextSize(TxtSize);
    SEWB_Tft_Lcd.println(Txt); 
}

bool SEWB_Tftlcd::SEWB_Tftlcd_Load_Start_Up_Logo(void)
{
	bool rResult = false;
	
	SEWB_Tft_Lcd.setRotation(1);
	SEWB_Tft_Lcd.setCursor(50, 150);
    SEWB_Tft_Lcd.fillScreen(TFT_DARKGREY);
	SEWB_Tftlcd_Write("GS-SOLUTIONS", 5, RED);
	delay(2000);
	SEWB_Tft_Lcd.setCursor(50, 100);
    SEWB_Tft_Lcd.fillScreen(TFT_DARKGREY);
	SEWB_Tftlcd_Write("SEWB", 7, GREEN);
    SEWB_Tft_Lcd.setCursor(50, 170);
	SEWB_Tftlcd_Write("SMART E-Waste Bin", 3, YELLOW);
    SEWB_Tft_Lcd.setCursor(50, 200);
    SEWB_Tftlcd_Write("Loading...", 1, RED);
	
	return rResult = true;
}
