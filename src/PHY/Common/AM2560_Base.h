#ifndef __AM2560_Base_H__
#define __AM2560_Base_H__

#include "AM2560.h"
#include "Arduino.h"

class AM2560_Base
{
	public:
		// CONSTRUCTOR
		AM2560_Base(void);
		
		// PUBLIC METHODS
		virtual bool AM2560_Open(void);
		virtual int AM2560_Read(int* pRead);
		virtual int AM2560_Write(void* pValue);
		virtual int AM2560_Ioctl(int* pBuff);
		virtual bool AM2560_Close(void);

		// GETTERS AND SETTERS

		// DESTRUCTOR
		virtual ~AM2560_Base(void);
		
	private:
		// NONE
};

#endif /* __AM2560_Base_H__ */