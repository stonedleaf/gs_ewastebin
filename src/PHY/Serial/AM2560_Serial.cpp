
#include "AM2560_Serial.h"

//#define DBG_SER 

#ifdef DBG_SER
	#define DBG_MSG_SER(MSG) pSerial->println(MSG)
#else
	#define DBG_MSG_SER(MSG) MSG
#endif

/* GETTERS AND SETTERS FUNC DEFINITION */
int AM2560_Serial::AM2560_Serial_set_Baud(int Baud)
{
	int rReturn = 0;
	
	Baud_Rate = Baud;
	
	return rReturn;
}

int AM2560_Serial::AM2560_Serial_get_Baud(void)
{
	return Baud_Rate;
}

int AM2560_Serial::AM2560_Serial_set_Wr_Type(int type)
{
	int rReturn = 0;

	Wr_Type = type;
	
	return rReturn;
}

int AM2560_Serial::AM2560_Serial_get_Available_Read(void)
{
	int rReturn_Val = 0;
	rReturn_Val = pSerial->available();
	return rReturn_Val;
}

/* CONSTRUCTOR DEFINITION */
AM2560_Serial::AM2560_Serial(HardwareSerial *S_port)
{
	/* Load Defaults */
	pSerial = S_port;
	AM2560_Serial_set_Baud(9600);
}

/* DESTRUCTOR DEFINITION */
AM2560_Serial::~AM2560_Serial(void)
{
	pSerial = NULL;
	AM2560_Serial_set_Baud(0);
}

/* START OF PUBLIC METHOD */
bool AM2560_Serial::AM2560_Open(void)
{
	bool rReturn_flg = false;
	pSerial->begin(AM2560_Serial_get_Baud());
	DBG_MSG_SER("AM2560_Driver_Open...");
	Serial.println(AM2560_Serial_get_Baud());
	return rReturn_flg = true;
}

bool AM2560_Serial::AM2560_Close(void)
{
	bool rReturn_flg = false;
	pSerial->end();
	return rReturn_flg = true;
}

int AM2560_Serial::AM2560_Read(int* pRead)
{
	int rReturn_Val 	= 0;
	
	DBG_MSG_SER("AM2560_Driver_Read...");
	
	if(nullptr != pRead)
	{
		*pRead = pSerial->read();
	}
	else
	{
		rReturn_Val = 1;
		DBG_MSG_SER("Error in Printing");
	}
	
	return rReturn_Val;
}

int AM2560_Serial::AM2560_Write(void* pValue)
{
	int 	rReturn_Val = 0;
	char* 	Str_Val 	= NULL;

	DBG_MSG_SER("AM2560_Driver_Write...");

	//if(nullptr != pValue)
	{
		switch(Wr_Type)
		{
			case WRT_INT:
				rReturn_Val = pSerial->println((int)pValue);
			break;
			case WRT_CHAR:
				rReturn_Val = pSerial->print((char)pValue);
			break;
			case WRT_STRING:
				Str_Val = (char*)pValue;
				rReturn_Val = pSerial->println(Str_Val);
			break;
			case WRT_INT_FMT:
				pSerial->println((int)pValue,2); //FMT BIN:2, DEC:10, HEX:16, OCT:8
			break;
			case WRT_BYTE:
				rReturn_Val = pSerial->write((uint8_t)pValue);
				break;
			default:
				pSerial->println("Error in Printing");
			break;
		}
	}
	//else
	{
		DBG_MSG_SER("Error in Printing");
	}
	
	return rReturn_Val;
}

int AM2560_Serial::AM2560_Ioctl(int* pBuff)
{
	// [OPCODE][CMD][DATA]
	
	int rReturn = 0;
	IOCTL_Buff *IOBuff = new IOCTL_Buff;
	int pcnt = 0;
	static int *l_buffer;
	
	//DBG_MSG_SER("AM2560_Driver_IOCTL...");
	
	if(nullptr != pBuff)
	{
		l_buffer = pBuff;
		IOBuff->OPCode = l_buffer[pcnt++];
		IOBuff->Cmd = l_buffer[pcnt++];
		IOBuff->Data1 = l_buffer[pcnt++];
		IOBuff->Data2 = l_buffer[pcnt++];

		switch(IOBuff->OPCode)
		{
			case OPC_GET: // create a function table for this
			{
				switch(IOBuff->Cmd)
				{
					case 1:
						IOBuff->Data1 = AM2560_Serial_get_Available_Read();
					break;
					case 2:
						IOBuff->Data1 = AM2560_Serial_get_Baud();
					break;
				}
			}
			break;
			case OPC_SET: //
			{
				switch(IOBuff->Cmd)
				{
					case 1:
						IOBuff->Data1 = AM2560_Serial_set_Wr_Type(IOBuff->Data1);
					break;
					case 2:
						if (IOBuff->Data1 <= 0) {
							IOBuff->Data1 = 9600;
						}
						//IOBuff->Data1 = AM2560_Serial_set_Baud(IOBuff->Data1);
						AM2560_Serial_set_Baud(IOBuff->Data1);
					break;
				}
			}
			break;
			
			default:
			break;
		}
		
		pcnt = 0;
		l_buffer[pcnt++] = IOBuff->OPCode;
		l_buffer[pcnt++] = IOBuff->Cmd;
		l_buffer[pcnt++] = IOBuff->Data1;
		l_buffer[pcnt++] = IOBuff->Data2;
		pBuff = l_buffer;	
	}
	else
	{
		pSerial->println("Error in Printing");
	}
	
	delete IOBuff;
	return rReturn;
}





