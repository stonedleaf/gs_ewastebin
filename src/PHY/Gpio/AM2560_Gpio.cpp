
#include "AM2560_Gpio.h"

int AM2560_Gpio::AM2560_Set_Pin_Dir(uint8_t Pin, uint8_t Dir)
{
	int rResult = 0;
	
	if((Dir > 0) && (Dir < 3))
	{
		switch(Dir)
		{
			case AM2560_PIN_IN:
				pinMode(Pin,INPUT);
			break;
			case AM2560_PIN_OUT:
				pinMode(Pin,OUTPUT);
			break;
			default:
			break;
		}
	}
	else
	{
		rResult = 1;
	}
}

int AM2560_Gpio::AM2560_Set_Pin_Value(uint8_t Pin, uint8_t Val)
{
	int rResult = 0;
	
	if((Val > 0) && (Val < 1))
	{
		switch(Val)
		{
			case AM2560_PIN_LOW:
				digitalWrite(Pin,LOW);
			break;
			case AM2560_PIN_HIGH:
				digitalWrite(Pin,HIGH);
			break;
			default:
			break;
		}
	}
	else
	{
		rResult = 1;
	}
}

int AM2560_Gpio::AM2560_Get_Pin_Value(int Pin)
{
	int rResult = 0;
	rResult = digitalRead(Pin);
	return rResult;
}

AM2560_Gpio::AM2560_Gpio(void)
{
	//Empty
}

AM2560_Gpio::~AM2560_Gpio(void)
{
	// Empty
}

bool AM2560_Gpio::AM2560_Open(void)
{
	return true;
}

int AM2560_Gpio::AM2560_Read(int* pRead)
{
	int rReturn_Val 	= 0;
	
	if(nullptr != pRead)
	{
		*pRead = AM2560_Get_Pin_Value(*pRead);
	}
	else
	{
		rReturn_Val = 1;
	}
	
	return rReturn_Val;
}

int AM2560_Gpio::AM2560_Write(void* pValue)
{
	int 	rReturn_Val = 0;
	uint8_t *Gpio_PinVal;

	if(nullptr != pValue)
	{
		Gpio_PinVal = (uint8_t*)pValue;
		rReturn_Val = AM2560_Set_Pin_Value(Gpio_PinVal[0],Gpio_PinVal[1]);
	}
	
	return rReturn_Val;	
}

int AM2560_Gpio::AM2560_Ioctl(int* pBuff)
{
	// [OPCODE][CMD][DATA]
	
	int rReturn = 0;
	IOCTL_Buff *IOBuff = new IOCTL_Buff;
	int pcnt = 0;
	static int *l_buffer;
	
	if(nullptr != pBuff)
	{
		l_buffer = pBuff;
		IOBuff->OPCode = l_buffer[pcnt++];
		IOBuff->Cmd = l_buffer[pcnt++];
		IOBuff->Data1 = l_buffer[pcnt++];
		IOBuff->Data2 = l_buffer[pcnt++];

		switch(IOBuff->OPCode)
		{
			case OPC_GET: // create a function table for this
			{
				switch(IOBuff->Cmd)
				{
					default:
					break;
				}
			}
			break;
			case OPC_SET: //
			{
				switch(IOBuff->Cmd)
				{
					case 1:
						IOBuff->Data1 = AM2560_Set_Pin_Dir((uint8_t)IOBuff->Data1,(uint8_t)IOBuff->Data2);
					break;
					default:
					break;
				}
			}
			break;
			
			default:
			break;
		}
		
		pcnt = 0;
		l_buffer[pcnt++] = IOBuff->OPCode;
		l_buffer[pcnt++] = IOBuff->Cmd;
		l_buffer[pcnt++] = IOBuff->Data1;
		l_buffer[pcnt++] = IOBuff->Data2;
		pBuff = l_buffer;	
	}
	
	delete IOBuff;
	return rReturn;	
}

bool AM2560_Gpio::AM2560_Close(void)
{
	return true;		
}
