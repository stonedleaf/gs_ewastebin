
#include "SEWB_Gpio_Ir.h"

SEWB_Gpio_Ir::SEWB_Gpio_Ir(void)
{
	pAMGpio = new AM2560_IF(1,NULL); // 1 for GPIO
}

SEWB_Gpio_Ir::~SEWB_Gpio_Ir(void)
{
	delete pAMGpio;
}

bool SEWB_Gpio_Ir::SEWB_Gpio_Open(void)
{
	bool rReturn_flg = false;
	rReturn_flg = pAMGpio->AM2560_Open();
	return rReturn_flg;
}

bool SEWB_Gpio_Ir::SEWB_Gpio_Close(void)
{
	bool rReturn_flg = false;
	rReturn_flg = pAMGpio->AM2560_Close();
	return rReturn_flg;
}

int SEWB_Gpio_Ir::SEWB_Gpio_Write(void)
{
	return 0;
}

int SEWB_Gpio_Ir::SEWB_GPIO_Read(uint8_t Pin)
{
	int Gpio_ReadVal = Pin;
	
	pAMGpio->AM2560_Read(&Gpio_ReadVal);

	return Gpio_ReadVal;
}

bool SEWB_Gpio_Ir::SEWB_GPIO_Set_Dir(uint8_t Pin, uint8_t Dir)
{
	int rReturn = 0;
	int Gpio_Dir_Packets[4] = {2,1,Pin,Dir};
	
	rReturn = pAMGpio->AM2560_Ioctl(Gpio_Dir_Packets);
	
	return (rReturn == 0)? true:false;
}
