#ifndef __SEWB_GPIO_IR_H__
#define __SEWB_GPIO_IR_H__

#include "../../PHY/AM2560_IF.h"

class SEWB_Gpio_Ir
{
	public:
		// CONSTRUCTOR
		SEWB_Gpio_Ir(void);
		
		// PUBLIC METHODS
		bool SEWB_Gpio_Open(void);
		bool SEWB_Gpio_Close(void);
		int SEWB_Gpio_Write(void);
		int SEWB_GPIO_Read(uint8_t Pin);
		bool SEWB_GPIO_Set_Dir(uint8_t Pin, uint8_t Dir);
		
		// DESTRUCTOR
		virtual ~SEWB_Gpio_Ir(void);
	private:
		// PRIVATE VARIABLES
		AM2560_IF* pAMGpio;
};

#endif /* __SEWB_GPIO_IR_H__ */
