#ifndef __SEWB_SERIAL_H__
#define __SEWB_SERIAL_H__

#include "../../PHY/AM2560_IF.h"

class SEWB_Serial
{
	public:
		// CONSTRUCTOR
		SEWB_Serial(HardwareSerial *S_port);
		
		// PUBLIC METHODS
		bool SEWB_Serial_Open(int rate = 9600);
		bool SEWB_Serial_Close(void);
		int SEWB_Serial_Print(int Type, void* Msg);
		int SEWB_Serial_Read(void);
		int SEWB_Serial_Available_Read(void);
		// GETTERS AND SETTERS

		
		// DESTRUCTOR
		~SEWB_Serial(void);
		
	private:
		// PRIVATE VARIABLES
		AM2560_IF* pAMSerial;
};

#endif /* __SEWB_SERIAL_H__ */
