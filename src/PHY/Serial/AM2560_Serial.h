#ifndef __AM2560_SERIAL_H__
#define __AM2560_SERIAL_H__

#include "../Common/AM2560_Base.h"

class AM2560_Serial : public AM2560_Base
{
	public:
		// CONSTRUCTOR
		AM2560_Serial(HardwareSerial *S_Port);
		
		// PUBLIC METHODS
		bool AM2560_Open(void) 			override;
		int AM2560_Read(int* pRead) 	override;
		int AM2560_Write(void* pValue) 	override;
		int AM2560_Ioctl(int* pBuff) 	override;
		bool AM2560_Close(void) 		override;
		
		// DESTRUCTOR
		virtual ~AM2560_Serial(void);
		
	private:
		// PRIVATE VARIABLES
		HardwareSerial *pSerial;
		int Baud_Rate = 0;
		int Wr_Type = 0;
		
		typedef struct _IOCTL_Buff
		{
			uint8_t OPCode;
			uint8_t Cmd;
			int Data1;
			int Data2;
		}IOCTL_Buff;
		
		// PRIVATE FUNCTIONS GETTERS AND SETTERS
		int AM2560_Serial_get_Available_Read(void);			// 1
		int AM2560_Serial_get_Baud(void);					// 2

		int AM2560_Serial_set_Wr_Type(int type);			// 1
		int AM2560_Serial_set_Baud(int Baud);				// 2
		
};

#endif /* __AM2560_Serial_H__ */

