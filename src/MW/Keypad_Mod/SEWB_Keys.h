#ifndef SEWB_KEYPAD_H
#define SEWB_KEYPAD_H

#include <stdbool.h>
#include "Keypad.h"

class SEWB_keys
{
	public:
		// CONSTRUCTOR
		SEWB_keys(void);
		
		// METHODS
		bool SEWB_KEY_Get_Key_Pressed(uint8_t*);

		//DESTRUCTOR
		~SEWB_keys(void);
	private:
		static const uint8_t Keypad_Row = 4; // four rows
		static const uint8_t Keypad_Col = 3; // three columns
		
		uint8_t rowPins[Keypad_Row] = {50, 51, 52, 53};  //connect to the row pinouts of the keypad
		uint8_t colPins[Keypad_Col] = {47, 48, 49};      //connect to the column pinouts of the keypad
		
		char keys[Keypad_Row][Keypad_Col] = 
		{
			{'1','2','3'},
			{'4','5','6'},
			{'7','8','9'},
			{'*','0','#'}
		};
		
		Keypad* keypad;
};


#endif /* SEWB_KEYPAD_H */