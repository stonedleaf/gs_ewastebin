/*
* Wifi Library
* 
* Author: Jim I.
* April 2021
*
*/

#include "SEWB_Wifi.h"

#ifdef PROGMEM
#undef PROGMEM
#define PROGMEM __attribute__((section(".progmem.data")))
#endif

const char REP_OK[] PROGMEM = "OK";
const char REP_NO_IP[] PROGMEM = "0.0.0.0";

const char REP_CMD_GETFIRM[] PROGMEM = "AT+RST";

SEWB_Wifi::SEWB_Wifi(SEWB_Serial *serialWifi, SEWB_Serial *serialDebug = NULL, int8_t resetPin = -1) : 
    _serialWifi(serialWifi),
    _serialDebug(serialDebug),
    _resetPin(resetPin) 
{
    pinMode(_resetPin, OUTPUT);
    digitalWrite(_resetPin, LOW);//Start with radio off
}

bool SEWB_Wifi::begin()
{
    // get status
    bool bStatus = false;
    byte i;
    for (i = 0; i < HW_RESET_RETRIES; i++) {
        readCommand(10, REP_NO_IP); //Cleanup
        digitalWrite(_resetPin, LOW);
        delay(500);
        digitalWrite(_resetPin, HIGH); // select the radio
        // Look for ready string from wifi module
        println(REP_CMD_GETFIRM);
        bStatus = readCommand(3000, REP_OK) == 1;
        if(bStatus)
            break;
    }

    if (!bStatus)
        return false;

    return true;
}

byte SEWB_Wifi::readCommand(int timeout, const char* text1, const char* text2) 
{
    // setup buffers on stack & copy data from PROGMEM pointers
    char buf1[16] = {'\0'};
    char buf2[16] = {'\0'};
    if (text1 != NULL)
        strcpy_P(buf1, (char *) text1);
    if (text2 != NULL)
        strcpy_P(buf2, (char *) text2);
    byte len1 = strlen(buf1);
    byte len2 = strlen(buf2);
    byte pos1 = 0;
    byte pos2 = 0;

    // read chars until first match or timeout
    unsigned long stop = millis() + timeout;
    do {
        while (_serialWifi->SEWB_Serial_Available_Read()) {
            char c = _serialWifi->SEWB_Serial_Read();
            //_serialDebug->SEWB_Serial_Print(WRT_CHAR, c);
            pos1 = (c == buf1[pos1]) ? pos1 + 1 : 0;
            pos2 = (c == buf2[pos2]) ? pos2 + 1 : 0;
            if (len1 > 0 && pos1 == len1)
                return 1;
            if (len2 > 0 && pos2 == len2)
                return 2;
        }
        delay(10);
    } while (millis() < stop);
    return 0;
}

size_t SEWB_Wifi::write(uint8_t c) 
{
  if (_serialDebug) {
    if (!_writingToDebug) {
        _serialDebug->SEWB_Serial_Print(WRT_STRING, "---> ");

        _writingToDebug = true;
    }
    _serialDebug->SEWB_Serial_Print(WRT_BYTE, c);
  }
  return _serialWifi->SEWB_Serial_Print(WRT_BYTE, c);
}
