#ifndef SEWB_TFTLCD_H
#define SEWB_TFTLCD_H

#include <stdbool.h>
#include "Adafruit_GFX.h"// Hardware-specific library
#include "MCUFRIEND_kbv.h"

class SEWB_Tftlcd
{
	public:
		// CONSTRUCTOR
		SEWB_Tftlcd(void);
		
		// METHODS
		bool SEWB_Tftlcd_Begin(void);
		bool SEWB_Tftlcd_Reset(void);
		void SEWB_Tftlcd_FillScreen(int bColor);
		void SEWB_Tftlcd_Write(String Txt, int TxtSize, int TxtColor);
		bool SEWB_Tftlcd_Load_Start_Up_Logo(void);

		//DESTRUCTOR
		~SEWB_Tftlcd(void);
	private:
		MCUFRIEND_kbv SEWB_Tft_Lcd;
		uint16_t LCD_ID = 0;
		
};


#endif /* SEWB_TFTLCD_H */