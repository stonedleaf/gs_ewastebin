#ifndef __AM2560_IF_H__
#define __AM2560_IF_H__

#include "Common/AM2560_Base.h"
#include "Serial/AM2560_Serial.h"
#include "Gpio/AM2560_Gpio.h"

class AM2560_IF : public AM2560_Base
{
	public:
		// CONSTRUCTOR
		AM2560_IF(uint8_t Dev_Periph = AM2560_UART, void* Per = nullptr);
		
		// PUBLIC METHODS
		bool AM2560_Open(void) 			override;
		int AM2560_Read(int* pRead) 	override;
		int AM2560_Write(void* pValue) 	override;
		int AM2560_Ioctl(int* pBuff) 	override;
		bool AM2560_Close(void) 		override;

		// DESTRUCTOR
		virtual ~AM2560_IF(void);
		
	private:
		AM2560_PROTOCOL Periph;
		AM2560_Base* AM2560_Periph;
};

#endif /* __AM2560_IF_H__ */