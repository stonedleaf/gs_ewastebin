#ifndef __AM2560_GPIO_H__
#define __AM2560_GPIO_H__

#include "../Common/AM2560_Base.h"

class AM2560_Gpio : public AM2560_Base
{
	public:
		// CONSTRUCTOR
		AM2560_Gpio(void);
		
		// PUBLIC METHODS
		bool AM2560_Open(void) 			override;
		int AM2560_Read(int* pRead) 	override;
		int AM2560_Write(void* pValue) 	override;
		int AM2560_Ioctl(int* pBuff) 	override;
		bool AM2560_Close(void) 		override;
		
		// DESTRUCTOR
		virtual ~AM2560_Gpio(void);
	
	private:
		// PRIVATE VARIABLES
		typedef struct _IOCTL_Buff
		{
			uint8_t OPCode;
			uint8_t Cmd;
			uint32_t Data1;
			uint32_t Data2;
		}IOCTL_Buff;
		
		// PRIVATE FUNCTIONS
		AM2560_Set_Pin_Dir(uint8_t Pin, uint8_t Dir);
		AM2560_Set_Pin_Value(uint8_t Pin, uint8_t Val);
		AM2560_Get_Pin_Value(int Pin);

		
	
};


#endif /* __AM2560_GPIO_H__ */
