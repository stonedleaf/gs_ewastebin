
#include "SEWB_Serial.h"


SEWB_Serial::SEWB_Serial(HardwareSerial *S_port)
{
	pAMSerial = new AM2560_IF(0,S_port); // 0 for UART
}

SEWB_Serial::~SEWB_Serial(void)
{
	delete pAMSerial;
}

bool SEWB_Serial::SEWB_Serial_Open(int rate)
{
	bool rReturn_flg = false;
	int Baud_Packets[4] = {2,2,rate,0};
	
	pAMSerial->AM2560_Close();
	pAMSerial->AM2560_Ioctl(Baud_Packets);
	rReturn_flg = pAMSerial->AM2560_Open();

	Serial.println("open");
	Serial.println(rate);
	Serial.println(Baud_Packets[2]);
	Serial.println("ed");
	
	return rReturn_flg;
}

bool SEWB_Serial::SEWB_Serial_Close(void)
{
	bool rReturn_flg = false;
	
	rReturn_flg = pAMSerial->AM2560_Close();
	
	return rReturn_flg ;
}

int SEWB_Serial::SEWB_Serial_Print(int Type, void* Msg)
{
	int Serial_WrType_Packets[4] = {2,1,Type,0};
	
	pAMSerial->AM2560_Ioctl(Serial_WrType_Packets);
	
	return pAMSerial->AM2560_Write(Msg);
}

int SEWB_Serial::SEWB_Serial_Read(void)
{
	int rReturn = 0;
	int read_Val = 0;
	
	rReturn = pAMSerial->AM2560_Read(&read_Val);
	
	return read_Val;
}

int SEWB_Serial::SEWB_Serial_Available_Read(void)
{
	int rReturn = 0;
	int Serial_Avail_Packets[4] = {1,1,0,0};
	
	rReturn = pAMSerial->AM2560_Ioctl(Serial_Avail_Packets);
	
	return Serial_Avail_Packets[2];
}



