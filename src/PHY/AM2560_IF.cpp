
#include "AM2560_IF.h"


AM2560_IF::AM2560_IF(uint8_t Dev_Periph, void* Per)
{
	
	switch(Dev_Periph)
	{
		case AM2560_UART:
			AM2560_Periph = new AM2560_Serial((HardwareSerial*)Per);
		break;
		case AM2560_GPIO:
			AM2560_Periph = new AM2560_Gpio();
		break;
	}
}

AM2560_IF::~AM2560_IF(void)
{
	// Empty
	AM2560_Periph = NULL;
}

bool AM2560_IF::AM2560_Open(void)
{
	return AM2560_Periph->AM2560_Open();
}

int AM2560_IF::AM2560_Read(int* pRead)
{
	return AM2560_Periph->AM2560_Read(pRead);
}

int AM2560_IF::AM2560_Write(void* pValue)
{
	return AM2560_Periph->AM2560_Write(pValue);		
}

int AM2560_IF::AM2560_Ioctl(int* pBuff)
{
	return AM2560_Periph->AM2560_Ioctl(pBuff);		
}

bool AM2560_IF::AM2560_Close(void)
{
	return AM2560_Periph->AM2560_Close();		
}
