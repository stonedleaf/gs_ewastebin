/* 
|| @file    : SEWB.ino
|| @version : 1.0
|| @author  : Eric Pena
|| @contact : ericpena03@gmail.com
||
|| @description
|| | The Application tends to control the smart E-Waste Bin
|| #
||
|| @history
|| * 2021-04-01     Jim Ilejay      Added WiFi Features
 */

/****************** Library Inclusion ******************/
#include "src/MW/Serial_Mod/SEWB_Serial.h"
#include "src/MW/GPIO_Ir_Mod/SEWB_GPIO_IR.h"
#include "src/MW/Keypad_Mod/SEWB_Keys.h"
#include "src/MW/TFTLCD_Mod/SEWB_TFTLCD.h"
#include "src/MW/WIFI_Mod/SEWB_Wifi.h"

/****************** CONSTANT Defines *******************/
#define WRT_INT       0
#define WRT_CHAR      1
#define WRT_STR       2

#define GPIO_DIR_IN   0
#define GPIO_DIR_OUT  1

#define SENSE_IR_PIN1 23
#define SENSE_IR_PIN2 25
#define SENSE_IR_PIN3 27

/******************* GLOBAL VARIABLES *******************/
bool SEWB_Diagnostic_flg = false;

/****************** Function Prototype ******************/
void SEWB_Init_Helper_Func(char* Mod, bool Sts);
void SEWB_Ir_Reader(void);
bool SEWB_Diag_Count_Dn();

/****************** Class declaration ******************/

SEWB_Serial SEWB_Ser(&Serial);    // Initialize Serial
SEWB_Tftlcd SEWB_LCD;
SEWB_keys SEWB_keypad;            // Initialize a 4x3 keypad
SEWB_Gpio_Ir SEWB_IR;

SEWB_Serial SEWB_SerWifi(&Serial2);    // Initialize Wifi Serial
SEWB_Wifi SEWB_WIFI(&SEWB_SerWifi, &SEWB_Ser, 39);

void setup() 
{
    bool rResult_LCD = false;
    bool rResult = false;
    /* Initialize SEWB Peripherals */

    rResult_LCD = SEWB_LCD.SEWB_Tftlcd_Begin();
    
    rResult = SEWB_Ser.SEWB_Serial_Open();
    SEWB_Init_Helper_Func("TFTLCD",rResult_LCD);
    SEWB_Init_Helper_Func("Serial",rResult);

    rResult = SEWB_SerWifi.SEWB_Serial_Open(115200);

    SEWB_LCD.SEWB_Tftlcd_Load_Start_Up_Logo();

    rResult = SEWB_IR.SEWB_Gpio_Open();
    SEWB_Init_Helper_Func("GPIO",rResult);

    rResult = SEWB_IR.SEWB_GPIO_Set_Dir(SENSE_IR_PIN1,GPIO_DIR_IN);
    SEWB_Init_Helper_Func("SENSE IR 1",rResult);

    rResult = SEWB_IR.SEWB_GPIO_Set_Dir(SENSE_IR_PIN2,GPIO_DIR_IN);
    SEWB_Init_Helper_Func("SENSE IR 2",rResult);

    rResult = SEWB_IR.SEWB_GPIO_Set_Dir(SENSE_IR_PIN3,GPIO_DIR_IN);
    SEWB_Init_Helper_Func("SENSE IR 3",rResult);

    rResult = SEWB_SerWifi.SEWB_Serial_Open(115200);
    SEWB_Init_Helper_Func("Wifi HW",rResult);
    rResult = SEWB_WIFI.begin();
    SEWB_Init_Helper_Func("WIFI Network",rResult);
    
    SEWB_Ser.SEWB_Serial_Print(WRT_STR,"Smart E-Waste Bin Initialize Complete !!!");

    // Check if the User wants to enter Diagnostics or start the main app.
    SEWB_Diagnostic_flg = SEWB_Diag_Count_Dn();
    (SEWB_Diagnostic_flg == true ) ? SEWB_Ser.SEWB_Serial_Print(WRT_STR,"Starting Diagnostic APP") : SEWB_Ser.SEWB_Serial_Print(WRT_STR,"Smart E-Waste Bin Application Started!!!");
    
}

void loop() 
{
    char loop_rx_byte[2] = {'\0'};
    char Key_press;
    int sum = 0;
    int Ser_Char_Avail = 0;

    if(SEWB_Diagnostic_flg == true)
    {
        SEWB_Ser.SEWB_Serial_Print(WRT_STR,"=============== Smart E-Waste DIAG ===============");
        SEWB_Ser.SEWB_Serial_Print(WRT_STR,"a. LCD                                            ");
        SEWB_Ser.SEWB_Serial_Print(WRT_STR,"b. IR GPIO                                        ");
        SEWB_Ser.SEWB_Serial_Print(WRT_STR,"c. Keypad                                         ");
        SEWB_Ser.SEWB_Serial_Print(WRT_STR,"d. Cellular                                       ");
        SEWB_Ser.SEWB_Serial_Print(WRT_STR,"e. WIFI                                           ");
        SEWB_Ser.SEWB_Serial_Print(WRT_STR,"x. Exit                                           ");
        SEWB_Ser.SEWB_Serial_Print(WRT_STR,"==================================================");
        SEWB_Ser.SEWB_Serial_Print(WRT_STR,"Please Choose: ");

        while(SEWB_Ser.SEWB_Serial_Available_Read() != 2);

        Ser_Char_Avail = SEWB_Ser.SEWB_Serial_Available_Read();
        if (Ser_Char_Avail == 2)
        {       
            for( int x = 0; x < Ser_Char_Avail; x++)
            {
                loop_rx_byte[x] = SEWB_Ser.SEWB_Serial_Read();
            }
            
            if(loop_rx_byte[0] != ' ')
            {
                switch(loop_rx_byte[0])
                {
                    case 'a':
                      SEWB_Ser.SEWB_Serial_Print(WRT_STR,"LCD Diagnostic Started");
                    break;
                    case 'b':
                      SEWB_Ser.SEWB_Serial_Print(WRT_STR,"GPIO IR Diagnostic Started");
                    break;
                    case 'c':
                      SEWB_Ser.SEWB_Serial_Print(WRT_STR,"Keypad Diagnostic Started");
                    break;
                    case 'd':
                      SEWB_Ser.SEWB_Serial_Print(WRT_STR,"Cellular Diagnostic Started");
                    break;
                    case 'e':
                      SEWB_Ser.SEWB_Serial_Print(WRT_STR,"WIFIU Diagnostic Started");
                    break;
                    default:
                      SEWB_Ser.SEWB_Serial_Print(WRT_STR,"Exiting...");
                      SEWB_Ser.SEWB_Serial_Print(WRT_STR,"Smart E-Waste Bin Application Started!!!");
                      SEWB_Diagnostic_flg = false;
                    break;
                }
            }
        }
    }
    else
    {
        if( SEWB_keypad.SEWB_KEY_Get_Key_Pressed(&Key_press) == true )
        {
            SEWB_Ser.SEWB_Serial_Print(WRT_CHAR,Key_press);
        }
        //SEWB_Ir_Reader();  
    }
}

bool SEWB_Diag_Count_Dn()
{
    static uint8_t cnt_dn = 5;
    bool SEWB_Diag_flg = false;
    int data_avail = 0;
    char Diag_Code = ' ';
    char Diag_RX_Code[2] = {'\0'};
    
    do
    {
        cnt_dn--;
        data_avail = SEWB_Ser.SEWB_Serial_Available_Read();
        if(data_avail > 0 )
        {
            for( int x = 0; x < data_avail; x++)
            {
                Diag_RX_Code[x] = SEWB_Ser.SEWB_Serial_Read();
            }
            if(Diag_RX_Code[0] == Diag_Code)
            {
                SEWB_Diag_flg = true;
                cnt_dn = 0;
            }
        }
        SEWB_Ser.SEWB_Serial_Print(WRT_STR,"Press Space Key to Enter Diagnostics...");
        delay(1000);
        
    }while(cnt_dn > 0);  
    return SEWB_Diag_flg;
}

void SEWB_Init_Helper_Func(char* Mod, bool Sts)
{
    char Mod_Str[50] = {'\0'};

    strcpy(Mod_Str,Mod);
    if(Sts == true) 
    {
        strcat(Mod_Str," Initialized...OK");
        SEWB_Ser.SEWB_Serial_Print(WRT_STR,Mod_Str);
    }
    else
    {
        strcat(Mod_Str," Initialized...NG");
        SEWB_Ser.SEWB_Serial_Print(WRT_STR,Mod_Str);
    }
}

void SEWB_Ir_Reader(void)
{
    int Ir_Value = 0;
    static int IR_Cntr = 0;
    
    Ir_Value = (SEWB_IR.SEWB_GPIO_Read(SENSE_IR_PIN3));
    Ir_Value |= (SEWB_IR.SEWB_GPIO_Read(SENSE_IR_PIN2)) << 1;
    Ir_Value |= (SEWB_IR.SEWB_GPIO_Read(SENSE_IR_PIN1)) << 2;

    //SEWB_Ser.SEWB_Serial_Print(WRT_INT,Ir_Value);
    
    switch(Ir_Value)
    {
        case 0:
            IR_Cntr++;
            if(IR_Cntr >= 1000)
            {
                SEWB_Ser.SEWB_Serial_Print(WRT_STR,"ALL IR is ENABLED!!!");
                IR_Cntr = 0;
            }
        break;
        case 1:
        break;
        case 2:
        break;
        case 3:
        break;
        case 4:
        break;
        case 5:
        break;
        case 6:
        break;
        case 7:
        break;

        default:
        break;
    }
}
